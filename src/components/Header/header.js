import "./header.scss";
import propTypes from "prop-types";
import { Link } from "react-router-dom";
const Header = props => {
  return (
    <header className="page-header">
      <div className="header-nav">
        <Link to='/'><i className="logo fa-brands fa-amazon"></i></Link>
        <div className="header-icons">
          <Link to='cart'><i className="cart fas fa-cart-plus"></i></Link>
          <p>{props.cart}</p>
          <Link to='favorites'><i className="like fas fa-heart"></i></Link>
          <p>{props.like}</p>
        </div>
      </div>
    </header>
  );
};
Header.propTypes = {
  cart: propTypes.number,
  like: propTypes.number,
};
Header.defaultProps = {
  cart: 0,
  like: 0,
};
export default Header;
