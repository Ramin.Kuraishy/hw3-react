import React, { useState, useEffect } from "react";
import ListItem from "./ListItem/listItem";
import "./list.scss";
import propTypes from "prop-types";

const List = props => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch("./data.json")
      .then(res => res.json())
      .then(result => setItems(state => (state.items = result)));
  }, []);

  return (
    <ul className="page-list">
      {items.map(item => (
        <ListItem
          key={item.article.toString()}
          item={item}
          items={items}
          setReadyToAddId={props.setReadyToAddId}
          showModal={props.showModal}
          favlike={props.favlike}
          btnIsActive={true}
          like={props.like}
        />
      ))}
    </ul>
  );
};
List.propTypes = {
  setReadyToAddId: propTypes.func,
  showModal: propTypes.func,
  favlike: propTypes.func,
};
List.defaultProps = {};
export default List;
